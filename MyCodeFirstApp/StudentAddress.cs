﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyCodeFirstApplication
{
    public class StudentAddress
    {
        public int StudentAddressId { get; set; }
        public String Address1 { get; set; }
        public String Address2 { get; set; }
        public virtual Student Student { get; set; }
    }
}
