﻿using MyCodeFirstApp;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Text;

namespace MyCodeFirstApplication
{
    public class FptSchoolContext : DbContext
    {
        //base() -> mặc định kết nối localDb
        //base("tên của Db")-> kết nối db local Sql Express có sẵn
        //base("name=tên của connection string")
        public FptSchoolContext() : base("name=FptDbCodeFirst")
        {
            Database.SetInitializer<FptSchoolContext>(new FptSchoolDbInitializer());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new StudentConfiguration());
            modelBuilder.Entity<Teacher>().ToTable("TeacherTable");
            modelBuilder.Entity<Teacher>().MapToStoredProcedures();
            base.OnModelCreating(modelBuilder);
        }
        public DbSet<Student> Students { get; set; }
        public DbSet<Grade> Grades { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<StudentAddress> StudentAddresses { get; set; }


    }
}
