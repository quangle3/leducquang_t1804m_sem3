﻿using MyCodeFirstApplication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyCodeFirstApp
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var ctx = new FptSchoolContext())
            {
                var student = new Student() { StudentName = "May" };
                ctx.Students.Add(student);
                ctx.SaveChanges();
            }
            Console.WriteLine("Create database code first successful");
            Console.ReadLine();
        }
    }
}
